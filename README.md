# Dungeon Password Manager

It's a simple, lightweight, mobile-enabled, web and CLI password manager/secrets storage based on Python3+Django2, inspired by Vault, Pass and KeePass.

As Vault, it uses a secret key shared by a number of parties to unseal the database, where it remains decrypted for a defined time or until the service is restarted.

Access is defined per user and per location, simply as read only or read+write. It has a web interface and a REST API for easy interaction with `curl` or other apps. Additionally, it runs as a CLI for command-line execution over SSH.

For storage, it uses Sqlite3, making it quite lightweigth but maybe unsuitable for environments that makes use of heavy concurrency.

**Note**: currently under developement!

## Characteristics

* Encrypted database: db remains sealed with a secret master key which is splitted (Shamir's secret sharing) amongst n certain users, and requires a number k of pieces to unseal it (it can't be used while sealed). Both n and k are configurable, having k<=n.
* Simple web interface: seal/unseal, log in/log out, search, add new entry, edit existing entry. Administration interface: configure permissions, other app settings.
* Entries have paths, acting as a directory structure.
* Entries have fields, and some fields are tagged as secrets.
* Each entry has a unique id, and each secret too.
* Users permissions set by administrator: -/r/rw based on entry path (even if the path doesn't exists yet).
* Entries available trough API allowing to pass secrets directly to other apps by piping: `curl -d 'api-otp=1cab...98dd' 'https://domain/api/v1/...' | gpg -d somefile.gpg`.
* To prevent unauthorize access to entries because of some shell history call to the API, tokens expire quickly (configurable, 30s by default).
* Optional long-term API tokens to allow interaction from other apps, using a different interface to avoid mistaking tokens: api-user and api-secret intead of api-otp, needing to call first the authorization API.

## Current milestones

- [ ] Secret master key (MK) shared and configurable on first boot (randomly generated).
- [ ] Optionally, encrypt MK piece (MK\_i) using PGP keys.
- [ ] Optionally, automatically email MK\_i to each user.
- [ ] MK change and new MK\_i generation (secrets rotation).
- [ ] Create root user on first boot, random password.
- [ ] Root password reset.
- [ ] User auth capability, log in/log out.
- [ ] Create users through admin interface.
- [ ] Create entries: name, username, password, url, notes.
- [ ] Entries additional secrets, attachments.
- [ ] Add, edit, delete, search entries.
- [ ] Configure users access permissions.
- [ ] Define API.
- [ ] Create API.
- [ ] Define CLI.
- [ ] Create CLI.
- [ ] Additionally encrypt secrets with PGP (every user must have a PGP key configured).
- [ ] External audit of key features: encryption, key shareing, authentication.

## Deploy

There's a ready to use deploy using Docker in its corresponding directory. For
developement purposes, simply run `./manage.py runserver`.

## Contributors

Special thanks to:

* @erus for coding and ideas.

## License

**Dungeon Password Manager** is made by [HacKan](https://hackan.net) under GNU GPL v3.0+. You are free to use, share, modify and share modifications under the terms of that [license](LICENSE).

    Copyright (C) 2018 HacKan (https://hackan.net)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

